from flask import Flask, render_template, request
import random

app = Flask(__name__)




@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        try:
            amount = int(request.form.get("amount"))
            first_number = int(request.form.get("number1"))
            second_number = int(request.form.get("number2"))
        except ValueError:
            try:
                return render_template('index.html', number="One or more inputs were invalid.", previous_amount=amount)
            except:
                return render_template("index.html", number="One or more inputs were invalid", previous_amount=1)

        if first_number > second_number:
            what_first_number_was = first_number
            first_number = second_number
            second_number = what_first_number_was
            
        string = str(random.randint(first_number, second_number))
        x=1
        while x < amount:
            string += ", " + str(random.randint(first_number, second_number))
            x=x+1
        number = string
        return render_template("index.html", number=number, previous_number1=first_number, previous_number2=second_number, previous_amount=amount)
    elif request.method == "GET":
        return render_template("index.html", number="Not generated yet!", previous_amount=1)


if __name__ == "__main__":
    app.run(debug=True)