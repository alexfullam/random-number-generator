# Random Number Generator

Video Demo: https://youtu.be/KdlAYZnHGNU

This is an application that uses Python's Flask library to create a simple web application that generates a certain amount of number (user-specified) between two user-specified numbers.

## Dependencies
- Python3
- Flask (a Python3 library that can be installed with pip, and this can be done in a virtualenv or on your system)

The project also uses HTML, CSS, and Bootstrap (no downloads required)

